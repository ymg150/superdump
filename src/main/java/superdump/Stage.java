package superdump;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

class Stage{
  MyPanel myPanel;
  double height, width;
  ListMyBall listball;
  Onore onore;
  int snum; // ステージ番号
  int time; // ステージタイム
  boolean vkRetry, vkPause;
  boolean isRetry = false;
  boolean isPause = false;
  boolean preVkP = false;
  Stage(MyPanel mp, int n){
    myPanel = mp;
    height = mp.getHeight();
    width = mp.getWidth();
    snum = n;

    retry();
  }
  double getHeight(){return height;}
  double getWidth(){return width;}
  void setRetry(boolean b){vkRetry = b;}
  void setPause(boolean b){vkPause = b;}
  boolean getRetry(){return vkRetry;}
  boolean getPause(){return vkPause;}

  void next(){
    if(isPause){
      pause();
    } else {
      step();
    }
    vkEvent();
  }
  void step(){
    time++;
    listball.next();
    onore.next();
    for(int i=listball.getBallNum()-1; i>=0; i--){
      for(int j=onore.listOnoBul.getBulNum()-1; j>=0; j--){
        if(Collision.ballBul(listball.list.get(i), onore.listOnoBul.list.get(j))){
          listball.list.get(i).setColor(Color.red); // Ball命中処理
          double x = onore.listOnoBul.list.get(j).getCenterX();
          double y = onore.listOnoBul.list.get(j).y + onore.listOnoBul.list.get(j).sizeY;
          listball.breakBall(i, x, y);
          onore.listOnoBul.hitBullet(j);
          break;
        }
      }
    }
    for(int i=listball.getBallNum()-1; i>=0; i--){
      MyBall mb = listball.list.get(i);
      onore.colliBall(mb);
    }
    listball.delBall();
  }
  void vkEvent(){
    if(!isPause){
      if(vkRetry && !isRetry){
        isRetry = true;
        retry();
      } else if(!vkRetry && isRetry){
        isRetry = false;
      }
    }
    if(vkPause && !preVkP){
      isPause = !isPause;
    }
    preVkP = vkPause;
  }
  void draw(Graphics g){
    listball.draw(g);
    onore.draw(g);
    g.drawString("time:"+time, 10, 10);
  }
  void restart(){}
  void retry(){
    listball = new ListMyBall(this);
    listball.addBallDir(width/2,height/2,50,Math.PI*300/180,10,Color.black);
    onore = new Onore(this, width/2-14, height-40);
    time = 0;
  }
  void gotoStage(int n){}
  void pause(){}
  void gameover(){}
}

// ボール初期状態など、ステージの情報を持つクラス
class ListStage{
//  List<Stage> list;
  MyPanel myPanel;
}
