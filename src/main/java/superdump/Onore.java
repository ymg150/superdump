package superdump;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class Onore{
  Stage stage;
  double width, height; // 画面サイズ
  double x, y;
  double sizeX, sizeY;
  double backupSx, backupSy;
  double speed; // 速度
  Color color = Color.blue;
  Color backupCol;
  boolean left, right, shoot, sliding;
  int moveDir, slideDir; // 0:停止 1:左 2:右
  ListOnoreBullet listOnoBul; // 弾
  int shotTimer = 0;
  int slideTimer = 0;
  boolean isSlide = false;
  boolean isReleaseSlide = true;
  Onore(Stage st, double x, double y){
    stage = st;
    width = stage.getWidth();
    height = stage.getHeight();
    this.x = x;
    this.y = y;
    sizeX = 28;
    sizeY = 40;
    speed = 10;

    left = false;
    right = false;
    shoot = false;
    sliding = false;
    moveDir = 0;

    listOnoBul = new ListOnoreBullet(stage);
  }

  void setLeft(){
    left = true;
    moveDir = 1;
  }
  void setRight(){
    right = true;
    moveDir = 2;
  }
  void setShoot(){shoot = true;}
  void setSliding(){sliding = true;}
  void setIsSlide(boolean b){isSlide = b;}
  void releaseLeft(){
    left = false;
    if(right) moveDir = 2;
    else moveDir = 0;
  }
  void releaseRight(){
    right = false;
    if(left) moveDir = 1;
    else moveDir = 0;
  }
  void releaseShoot(){shoot = false;}
  void releaseSliding(){
    sliding = false;
    isReleaseSlide = true;
  }
  boolean getLeft(){return left;}
  boolean getRight(){return right;}
  boolean getShoot(){return shoot;}
  boolean getSliding(){return sliding;}
  boolean getIsSlide(){return isSlide;}
  boolean getIsReleaseSlide(){return isReleaseSlide;}
  int getSlideDir(){return slideDir;}
  void setColor(Color col){color = col;}

  void draw(Graphics g){
    Color backup = g.getColor();
    g.setColor(color);
    g.fillRect((int)x, (int)y, (int)sizeX, (int)sizeY );
    g.setColor(backup);
    listOnoBul.draw(g);
  }
  void next(){
    if(moveDir == 1){
      x -= speed;
    } else if(moveDir == 2){
      x += speed;
    }
    if(slideTimer > 0){
      if(slideDir == 1){
        x -= speed * 1.5;
      } else if(slideDir == 2){
        x += speed * 1.5;
      }
      if(sliding){
        isReleaseSlide = false;
      }
      slideTimer--;
      if(slideTimer == 0){
        color = backupCol;
        if(slideDir == 1){
          x = x + sizeX - backupSx;
        }
        sizeX = backupSx;
        sizeY = backupSy;
        isSlide = false;
      }
    } else if(sliding && isReleaseSlide){
      if(moveDir != 0){
        slideDir = moveDir;
        slideTimer = 10;
        backupCol = color;
        color = Color.red;
        backupSx = sizeX;
        backupSy = sizeY;
        sizeX = backupSy;
        sizeY = backupSx;
        if(slideDir == 1){
          x = x + backupSx - sizeX;
        }
        isSlide = true;
      }
    }
    if(x < 0){x = 0;}
    else if(x+sizeX > width){x = width-sizeX;}
    y = height - sizeY;

    listOnoBul.next();
    if(shotTimer > 0){
      shotTimer--;
    } else if(shoot && listOnoBul.list.size() < 3){
      listOnoBul.shot(x+sizeX/2, y);
      shotTimer = 6;
    }
  }
  void colliBall(MyBall mb){
    if(Collision.cirRectSimple(mb.x, mb.y, mb.size, x, y, sizeX, sizeY)){
      if(isSlide){
        int dir = getSlideDir();
        double spx = 7;
        double spy = 15;
        if(dir == 1){
          mb.setMovXY(-spx,-spy);
        } else {
          mb.setMovXY(spx,-spy);
        }
      } else {
        setColor(Color.yellow); // 被弾処理
      }
    }
  }
}
