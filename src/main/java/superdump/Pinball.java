package superdump;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.applet.Applet;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

class Pinball extends JFrame {
  public static void main(String[] args) {
    JFrame f = new JFrame();
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.setSize(650, 400);
    //f.setSize(1000, 700);
    f.setTitle("Pinball");
    Container contentPane = f.getContentPane();
    MyPanel p = new MyPanel();
    contentPane.add(p);
    f.setVisible(true);
    p.start();
  }
}
class MyPanel extends JPanel implements ActionListener, KeyListener{
  ListMyBall listball;
  Onore onore;
  Stage stage;
  final Timer timer;

  public MyPanel() {
    timer = new Timer(33, this);
  }
  public void start() {
    stage = new Stage(this, 1);
    addKeyListener(this);
    timer.start();
  }
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    stage.draw(g);

    requestFocusInWindow();
  }
  public void actionPerformed(ActionEvent e) {
    stage.next();
    repaint();
  }

  public void keyPressed(KeyEvent e){
    int keycode = e.getKeyCode();
    if (keycode == KeyEvent.VK_RIGHT && !stage.onore.getRight()){
//			System.out.println("press right");
      stage.onore.setRight();
    } else if (keycode == KeyEvent.VK_LEFT && !stage.onore.getLeft()){
//			System.out.println("press left");
      stage.onore.setLeft();
    } else if (keycode == KeyEvent.VK_Z && !stage.onore.getShoot()){
//			System.out.println("press Z");
      stage.onore.setShoot();
    } else if (keycode == KeyEvent.VK_X && !stage.onore.getSliding()){
      System.out.println("press X");
      stage.onore.setSliding();
    } else if (keycode == KeyEvent.VK_R && !stage.getRetry()){
      stage.setRetry(true);
    } else if (keycode == KeyEvent.VK_P && !stage.getPause()){
      stage.setPause(true);
    }
  }

  public void keyReleased(KeyEvent e){
    int keycode = e.getKeyCode();
    if (keycode == KeyEvent.VK_RIGHT && stage.onore.getRight()){
//			System.out.println("released right");
      stage.onore.releaseRight();
    } else if (keycode == KeyEvent.VK_LEFT && stage.onore.getLeft()){
//			System.out.println("released left");
      stage.onore.releaseLeft();
    } else if (keycode == KeyEvent.VK_Z && stage.onore.getShoot()){
//			System.out.println("released Z");
      stage.onore.releaseShoot();
    } else if (keycode == KeyEvent.VK_X && stage.onore.getSliding()){
      System.out.println("released X");
      stage.onore.releaseSliding();
    } else if (keycode == KeyEvent.VK_R && stage.getRetry()){
      stage.setRetry(false);
    } else if (keycode == KeyEvent.VK_P && stage.getPause()){
      stage.setPause(false);
    }
  }

  public void keyTyped(KeyEvent e){
  }

}
