package superdump;

// 衝突判定をまとめたクラス
public class Collision{
  // 中心(x,y)半径rの円同士
  static boolean circle(double x1, double y1, double r1, double x2, double y2, double r2){
    /* (x1-x2)^2 + (y1-y2)^2 <= (r1+r2)^2 */
    if((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) <= (r1+r2)*(r1+r2)){
      return true;
    }
    return false;
  }

  // 中心(x1,y1)半径rの円と、左上(x2,y2),右下(x2+sx2,y2+sy2)の四角形
  static boolean cirRect(double x1, double y1, double r1, double x2, double y2, double sx2, double sy2){
    // 四角形の頂点が円の中にあるか
    if((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) <= r1*r1){
      return true;
    } else if((x1-x2-sx2)*(x1-x2-sx2) + (y1-y2)*(y1-y2) <= r1*r1){
      return true;
    } else if((x1-x2)*(x1-x2) + (y1-y2-sy2)*(y1-y2-sy2) <= r1*r1){
      return true;
    } else if((x1-x2-sx2)*(x1-x2-sx2) + (y1-y2-sy2)*(y1-y2-sy2) <= r1*r1){
      return true;
    }
    // 円の中心が四角形の中にあるか or 円と四角形の辺が重なっているか
    else if(x2 < x1 && x1 < x2+sx2 && y2 - r1 < y1 && y1 < y2 + sy2 + r1){
      return true;
    } else if(y2 < y1 && y1 < y2+sy2 && x2 - r1 < x1 && x1 < x2 + sx2 + r1){
      return true;
    }
    return false;
  }

  static boolean cirRectSimple(double x1, double y1, double r1, double x2, double y2, double sx2, double sy2){
    // 四角形の頂点が円の中にあるか
    if((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) <= r1*r1){
      return true;
    } else if((x1-x2-sx2)*(x1-x2-sx2) + (y1-y2)*(y1-y2) <= r1*r1){
      return true;
    } else if((x1-x2)*(x1-x2) + (y1-y2-sy2)*(y1-y2-sy2) <= r1*r1){
      return true;
    } else if((x1-x2-sx2)*(x1-x2-sx2) + (y1-y2-sy2)*(y1-y2-sy2) <= r1*r1){
      return true;
    }
    // 円の中心が四角形の中にあるか
    else if(x2 < x1 && x1 < x2+sx2  &&  y2 < y1 && y1 < y2+sy2){
      return true;
    }
    return false;
  }
  static boolean ballBul(MyBall ball, Bullet bul){
    if(bul.getDelFlag()){return false;}
    double ax = ball.x;
    double ay = ball.y;
    double as = ball.size;
    double bx = bul.x;
    double by = bul.y;
    double bsx = bul.sizeX;
    double bsy = bul.sizeY;
    if(Collision.cirRect(ax, ay, as, bx, by, bsx, bsy)){
      return true;
    }
    return false;
  }
}
